let map = L.map('map').setView([4.7, -74.13], 12);

let redIcon = L.icon.pulse({
    iconSize: [12, 12],
    color: 'red',
    fillColor: 'red'
});
let greenIcon = L.icon.pulse({
    iconSize: [12, 12],
    color: 'green',
    fillColor: 'green',
    animate: false
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

const myFeatureGroup = L.featureGroup().addTo(map).on("click", groupClick);

let marker = L.marker([4.723, -74.125], {
        icon: redIcon
    }).addTo(myFeatureGroup)
    .bindPopup('Nombre: Casa Gregorio<br/>Status: Error');

L.marker([4.6137759, -74.0638492], {
        icon: greenIcon
    }).addTo(myFeatureGroup)
    .bindPopup('Nombre: UD Macarena<br/>Status: Online');

function groupClick(event) {
    console.log("Clicked on marker " + event.layer.test);
}

let locLayer = L.geoJSON().addTo(map);

fetch('data/localidades.json')
    .then(response => response.json())
    .then(data => locLayer.addData(data));